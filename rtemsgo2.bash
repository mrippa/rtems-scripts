#!/bin/bash

RTEMS_BASE=/home/mrippa/work/rtems
if [ ! -d "$RTEMS_BASE" ]; then
   mkdir "RTEMS_BASE"
fi

cd "RTEMS_BASE"
mkdir rtems-src
cd rtems-src


#### prep
git clone git://git.rtems.org/rtems-source-builder.git rsb
cd rsb
git checkout 5
cd rtems
cd rsb/rtems

../source-builder/sb-set-builder --source-only-download 5/bsps/pc686

../source-builder/sb-set-builder --source-only-download 5/rtems-powerpc

../source-builder/sb-set-builder --source-only-download \
	--target=powerpc-rtems5 --with-rtems-bsp=mvme3100 \
	5/rtems-kernel 5/rtems-libbsd

#### execute

../source-builder/sb-set-builder --no-download \
	--prefix=$HOME/source/rtems/usr --log=i686-pc.txt \
   5/bsps/pc686

../source-builder/sb-set-builder \
   --no-download \
   --prefix=$HOME/source/rtems/usr --log=powerpc.txt \
   5/rtems-powerpc

../source-builder/sb-set-builder \
   --no-download \
   --prefix=$HOME/source/rtems/usr --log=powerpc-mvme3100.txt \
   --host=powerpc-rtems5 --target=powerpc-rtems5 --with-rtems-bsp=mvme3100 \
   5/rtems-kernel 5/rtems-libbsd

