#!/bin/bash
# To list available BSPs:
#run 'rtems-bsps' (in the top of the source tree) for a valid BSP list
set -e
set -x
#BSP=qoriq
#BSP="beatnik mvme2307 mvme3100 qemuppc"
BSP="pc686 mvme2307 mvme3100 qemuppc"
#PROCESSOR=powerpc
PROCESSOR=i386
#PROCESSOR=powerpc
# The directory to install RTEMS
RTEMS_VERSION=5
#RTEMS_PREFIX=/gem_base/targetOS/RTEMS/rtems-5
RTEMS_PREFIX=/gem_base/targetOS/RTEMS/rtems-$RTEMS_VERSION/rtems-5
# Location of the source directory of rtems source builder
RSB_SRC=/gem_base/targetOS/RTEMS/source/rtems-source-builder
# Location of the source directory of RTEMS
RTEMS_SRC=/gem_base/targetOS/RTEMS/source/rtems
if [ ! -d "$RSB_SRC" ]; then
    git clone git://git.rtems.org/rtems-source-builder.git "$RSB_SRC"
        if [ "$RTEMS_VERSION" == "5" ]; then
            git checkout -b r5 origin/5
        fi
fi
cd "$RSB_SRC"
git checkout master
git pull --all
git fetch --tags
if [ "$RTEMS_VERSION" == "5" ]; then
    git checkout r5
    git pull
else if [ "$RTEMS_VERSION" == "5.1" ]; then
        git checkout 5.1
   fi
fi
if [ ! -d "$RTEMS_SRC" ]; then
    git clone git://git.rtems.org/rtems.git "$RTEMS_SRC"
        if [ "$RTEMS_VERSION" == "5" ]; then
            git checkout -b r5 origin/5
        fi
fi
cd "$RTEMS_SRC"
git checkout master
git pull --all
git fetch --tags
if [ "$RTEMS_VERSION" == "5" ]; then
    git checkout r5 
    git pull
else if [ "$RTEMS_VERSION" == "5.1" ]; then
        git checkout 5.1
    fi
fi
# Build cross compile tool suite
cd "$RSB_SRC/rtems"
../source-builder/sb-set-builder --prefix="$RTEMS_PREFIX" "5/rtems-$PROCESSOR"
# Add cross compile tools to path
PATH="$RTEMS_PREFIX/bin:$PATH"
# Create a script to update path in shells.  Meant to be sourced via the shell.
# e.g. > source rtems-shell.env
echo "PATH=$RTEMS_PREFIX/bin:\$PATH" > rtems-shell.env
# Bootstrap the build system
cd "$RTEMS_SRC"
./bootstrap -c && ./rtems-bootstrap
cd "$RSB_SRC/rtems"
# Build BSP
for bsp in $BSP; do
../source-builder/sb-set-builder --prefix="$RTEMS_PREFIX" \
                                 --target="$PROCESSOR-rtems5" \
                                 --with-rtems-bsp="$bsp" \
                                 --with-rtems-tests=yes \
                                 5/rtems-kernel
done
# Test the BSP
# cd BSP dir?!
# rtems-test --rtems-bsp="$BSP" -rtems-tools="$RTEMS_PREFIX" .
# Build the BSP Stack
cd "$RSB_SRC"
# TODO need some way to customize the "pc" at the end of this
#../source-builder/sb-set-builder --prefix=$RTEMS_PREFIX --with-rtems-tests=yes $RTEMS_VERSION/bsps/pc
